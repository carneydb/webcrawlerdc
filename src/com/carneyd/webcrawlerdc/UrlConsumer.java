/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.carneyd.webcrawlerdc;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.function.Consumer;

/**
 *
 * @author carneyd
 */
public class UrlConsumer<String> implements Consumer {
    private String urlHost;
    // TODO: Exchange this for the HashSet
    private PageResult pageResult;
    
    // A consumer to filter URLs into the correct list
    public UrlConsumer(String urlHost, PageResult pageResult) {
        this.urlHost = urlHost;
        this.pageResult = pageResult;
    }
    
    public void accept(Object url) {
        try {
            URL toCheck = new URL(url.toString());
            if(urlHost.equals(toCheck.getHost())) {
                pageResult.getInternalUrls().add(url.toString());
            } else {
                pageResult.getExternalUrls().add(url.toString());
            }
        } catch(MalformedURLException mue) {
            // TODO: Perhaps could have a set with all invalid URLs
        }
    }
}
