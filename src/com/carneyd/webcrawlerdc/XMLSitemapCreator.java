/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.carneyd.webcrawlerdc;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.XMLOutputter;

/**
 *
 * @author carneyd
 */
public class XMLSitemapCreator {
    // Create and output an XML file from the Map of PageResults
    public static void createSitemap(String filename, HashMap<String, PageResult> pageResults) {
        Element root = new Element("SiteMap");
        Document doc = new Document(root);
        
        int count = 0;
        for(String key : pageResults.keySet()) {
            PageResult pr = pageResults.get(key);
            if(pr == null || pr.isIsValid() == false) {
                // TODO: Perhaps add the invalid and the ones that haven't been processed later.
                continue;
            }
            count++;
            Element pageCrawled = new Element("PageCrawled");
            pageCrawled.setAttribute("url", key);
            pageCrawled.setAttribute("count", count+"");
            
            Set<String> internalUrls = pr.getInternalUrls();
            for(String internalUrl : internalUrls) {
                Element intUrl = new Element("InternalUrl");
                intUrl.setText(internalUrl);
                pageCrawled.addContent(intUrl);
            }

            Set<String> externalUrls = pr.getExternalUrls();
            for(String externalUrl : externalUrls) {
                Element extUrl = new Element("ExternalUrl");
                extUrl.setText(externalUrl);
                pageCrawled.addContent(extUrl);
            }

            Set<String> staticContent = pr.getStaticContent();
            for(String staticCon : staticContent) {
                Element staticCont = new Element("StaticContent");
                staticCont.setText(staticCon);
                pageCrawled.addContent(staticCont);
            }
            
            root.addContent(pageCrawled);
        }
        
        System.out.println("Pages covered: "+count);
            
        XMLOutputter xmlOutputter = new XMLOutputter();
        try {
            xmlOutputter.output(doc, new FileOutputStream(filename));
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
