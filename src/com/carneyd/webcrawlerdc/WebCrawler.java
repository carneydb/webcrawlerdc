/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.carneyd.webcrawlerdc;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

/**
 *
 * @author carneyd
 */
public class WebCrawler {
    private String urlStringToStart, urlHost;
    private int maximumUrlsToCrawl;
    private HashMap<String, PageResult> pageResults = new HashMap<>();
    
    public WebCrawler(String urlStringToStart, int maximumUrlsToCrawl) {
        this.urlStringToStart = urlStringToStart;
        this.maximumUrlsToCrawl = maximumUrlsToCrawl;
    }
    
    // Use this to set up the URL host to check
    public void initialise() throws MalformedURLException {
        // Check the URL passed
        URL urlToStart = new URL(urlStringToStart);
        urlHost = urlToStart.getHost();
    }
    
    // Use this method to kick off the checking, using the url passed in the constructor.
    //  When the checking is complete for the initial page. 
    //  We can reassign the URL to the next unprocessed page on the HashMap.
    // And kick the checking off again.
    public void checkPage() {
        pageResults.put(urlStringToStart, null);
        PageChecker thisPageChecker = 
                new PageChecker(urlStringToStart, urlHost, pageResults);
        thisPageChecker.checkPage();
        
        int count = 0;
        // If there is another page to check and we haven't passed our maximum, scan this page.
        while(nextPageToCheck() != null && (maximumUrlsToCrawl-1) >= countPageResultsChecked()) {
            count++;
            
            System.out.println("Checking page "+count);
            thisPageChecker = new PageChecker(nextPageToCheck(), urlHost, pageResults);
            thisPageChecker.checkPage();
        }
        
        System.out.println("Creating the Sitemap XML...");
        String filename = "SiteMapFor"+urlHost+".xml";
        XMLSitemapCreator.createSitemap(filename, pageResults);
        System.out.println("Created the Sitemap XML. Called "+filename);
    }
    
    // Count how many pages have been processed
    private int countPageResultsChecked() {
        int count = 0;
        
        for(PageResult pr : pageResults.values()) {
            if(pr != null && pr.isProcessed() == true) {
                count++;
            }
        }
        
        return count;
    }
    
    // Get the next page to check, return null if none.
    private String nextPageToCheck() {
        for(String key : pageResults.keySet()) {
            PageResult pr = pageResults.get(key);
            if(pr == null) {
                return key;
            }
        }
        
        return null;
    }
}
