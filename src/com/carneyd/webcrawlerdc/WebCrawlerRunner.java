/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.carneyd.webcrawlerdc;

import java.net.MalformedURLException;

/**
 *
 * @author carneyd
 */
public class WebCrawlerRunner {
    private static final String DEFAULT_URL = "http://wiprodigital.com";
    private static final int DEFAULT_MAXIMUM = 10;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        // Check for a number being passed, if so, use as the count, anything else, use as the URL
        String urlStringToCheck = DEFAULT_URL;
        int maximumNumToCheck = DEFAULT_MAXIMUM;
        if(args.length > 0) {
            try {
                maximumNumToCheck = Integer.parseInt(args[0]);
                
                if(args.length > 1) {
                    urlStringToCheck = args[1];
                }
            } catch(NumberFormatException nfe) {
                urlStringToCheck = args[0];
                
                if(args.length > 1) {
                    try {
                        maximumNumToCheck = Integer.parseInt(args[1]);
                    } catch(NumberFormatException nfe1) {
                        maximumNumToCheck = DEFAULT_MAXIMUM;
                    }
                }
            }
        }
        
        System.out.println("Sitemap for "+urlStringToCheck+"\nFor at most "+maximumNumToCheck+" pages.");
        
        WebCrawler crawler = new WebCrawler(urlStringToCheck, maximumNumToCheck);
        
        boolean initialised = false;
        try {
            crawler.initialise();
            
            initialised = true;
            System.out.println("Initialised");
        } catch(MalformedURLException e) {
            System.err.println("URL passed was Invalid!");
            e.printStackTrace();
        }
        
        if(initialised == true) {
            System.out.println("Starting the Crawler...");
            crawler.checkPage();
            System.out.println("Finished the Crawler.");
        }
        
        long endTime = System.currentTimeMillis();
        long timeTaken = endTime - startTime;
        System.out.println("Time taken: "+(timeTaken / 1000)+" seconds.");
    }
    
}
