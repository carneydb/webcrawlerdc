/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.carneyd.webcrawlerdc;

import java.io.IOException;
import java.util.HashMap;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author carneyd
 */
public class PageChecker {
    private String urlStringToCheck, originalUrlHost;
    private HashMap<String, PageResult> pageResults;
    private UrlConsumer urlConsumer;
    
    public PageChecker(String urlStringToCheck, String originalUrlHost, HashMap<String, PageResult> pageResults) {
        this.urlStringToCheck = urlStringToCheck;
        this.originalUrlHost = originalUrlHost;
        this.pageResults = pageResults;
    }
    
    // Check the current page, create Page Results for it, add the internal sites that do not currently exist
    //  on the hashmap to it. Add the Page Results under the current page on the hashmap
    public void checkPage() {
        PageResult pageResult = new PageResult();
        urlConsumer = new UrlConsumer(originalUrlHost, pageResult);
        
        try {
            // Use JSoup to parse the page in question
            Document doc = Jsoup.connect(urlStringToCheck).get();
            
            // Scan all links in the page, use the absolute path
            doc.select("a[href]").forEach(link -> urlConsumer.accept(link.attr("abs:href")));
            
            // Scan all images on the page
            doc.select("img[src]").forEach(image -> pageResult.getStaticContent().add(image.attr("abs:src")));
            
            for(String internalUrl : pageResult.getInternalUrls()) {
                // If the internal URL does not already exist in the HashMap, 
                //  add this to the URLs to Check
                if(pageResults.containsKey(internalUrl) == false) {
                    pageResults.put(internalUrl, null);
                }
            }
            
            pageResult.setIsValid(true);
            pageResult.setProcessed(true);
        } catch(IOException e) {
            pageResult.setIsValid(false);
            pageResult.setProcessed(true);
//            e.printStackTrace();
        }
        
        if(!(pageResults.containsKey(urlStringToCheck) && 
                pageResults.get(urlStringToCheck) != null &&
                pageResults.get(urlStringToCheck).isProcessed() == true)) {
            pageResults.put(urlStringToCheck, pageResult);
        }
    }
}
