/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.carneyd.webcrawlerdc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author carneyd
 */
public class PageResult {
    private boolean processed = false, isValid = true;

    public boolean isIsValid() {
        return isValid;
    }

    public void setIsValid(boolean isValid) {
        this.isValid = isValid;
    }
    private Set<String> internalUrls = new HashSet<>(), 
            externalUrls = new HashSet<>(), 
            staticContent = new HashSet<>();

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public Set<String> getInternalUrls() {
        return internalUrls;
    }

    public void setInternalUrls(Set<String> internalUrls) {
        this.internalUrls = internalUrls;
    }

    public Set<String> getExternalUrls() {
        return externalUrls;
    }

    public void setExternalUrls(Set<String> externalUrls) {
        this.externalUrls = externalUrls;
    }

    public Set<String> getStaticContent() {
        return staticContent;
    }

    public void setStaticContent(Set<String> staticContent) {
        this.staticContent = staticContent;
    }
    
    public String toString() {
        return "PageResult [isProcessed ["+isProcessed()+"] "+
                "Internal URLs ["+getInternalUrls().size()+"] "+
                "External URLs ["+getExternalUrls().size()+"] "+
                "Static Content ["+getStaticContent().size()+"]";
    }
    
    public ArrayList<String> toCompleteString() {
        ArrayList<String> strings = new ArrayList<>();
        strings.add("Internal URLs:");
        for(String url : getInternalUrls()) {
            strings.add("\t"+url);
        }
        strings.add("External URLs:");
        for(String url : getExternalUrls()) {
            strings.add("\t"+url);
        }
        strings.add("Static Content:");
        for(String url : getStaticContent()) {
            strings.add("\t"+url);
        }
        return strings;
    }
}
