/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.carneyd.webcrawlerdc.WebCrawler;
import java.net.MalformedURLException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author carneyd
 */
public class WebCrawlerTest {
    
    public WebCrawlerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testInitialise_Success() {
        WebCrawler wc = new WebCrawler("https://www.baeldung.com/foreach-java", 10);
        try {
            wc.initialise();
        } catch(MalformedURLException e) {
            fail();
        }
    }
    
    @Test(expected=MalformedURLException.class)
    public void testInitialise_Fail() throws MalformedURLException {
        WebCrawler wc = new WebCrawler("", 10);
        wc.initialise();
    }
}
