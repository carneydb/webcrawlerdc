---
Running the Application
---
1. Go in to the dist directory.
2. Run the following:
java -jar "WebCrawlerDC.jar"
This program takes two additional optional parameters, a number for the number of webpages to scan, and the initial webpage URL to scan. 
These can be entered in any order. Not entering will result in the default number 10 and website http://wiprodigital.com
---
Building the Application
---
1. This was built using Java 8. The only dependencies that it has are JDOM 2.0.5 and JSoup 1.11.3, both are in the dist/lib directory.
2. I was building using NetBeans, so I have included the project files.
---
Reasoning / Trade Offs
---
This program takes two parameters, a limit of the pages to check (in case on a test run it ran away on me as I had no idea how complex the webpage was) and the site to crawl.
I wanted to keep a map for the results, so I used the URL of the site as a key, and a java object holding the internal, external and static content on them.
I used JSoup to parse the website and then an XPath query to find all img src's and a href's. The img src's went to static content and a href's were split between internal and external URLs.
I used Java 8 forEach on the XPaths to cycle through them, for the hrefs I used a consumer to split the files correctly on host name.
All Internal URLs were subsequently added to the HashMap.
The results are now added to the hashmap.
Then I checked of the hashmap had any entries that had not been processed or the count of processed urls was less than the limit. If that was the case we performed the scan for the next URL, otherwise we stopped looping.
At the end of looping the hashmap is sent to the XML File Creator.
The file created is called "SiteMapFor"+hostName+".xml"
---
What could have been done with more time
---
I would have liked to have provided more tests. There are two initialisation tests for the WebCrawler in the test directory.
I also would have liked to include the pages that hadn't been scanned (say as a result of the run running beyond the limit of pages) and invalid pages. Probably just the PageCrawled tag with a marker for each.
On the URL Consumer use an Invalid URLs set for links that create MalformedURLExceptions.
I would have also liked to scan for static content other than images.
This program cannot scan JavaScript to divine where links may be changed. This also could use a URL check.
If different hosts from the same company appear, say for example www.abc.com and jobs.abc.com, they will be classified as external sites.
---
Miscellaneous
---
The XML outputs of two runs have been included on the repository. A large file on the dist directory and a small one on the main ddirectory.